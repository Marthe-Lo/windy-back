# Windy-back


1. Projet
Le projet consiste à développer une application web pour une solution d’assurance paramétrique. Il s’agira de couvrir les pertes des éoliennes en cas d’absence de vent selon un seuil déterminé.
Par exemple, l’assuré pourra recevoir une indemnisation journalière de xxx euros pour chaque journée où l’on constate une absence de vent ou un vent faible en dessous du seuil défini (xx km/h).


2. Technologies

 - FRONT : utilisation du Framework Angular
La  partie front affiche des données issues du côté serveur : la connexion au back-end se réalise donc au moyen d’un API REST. Il est à noter que la partie client se connecte à 2 back-end : utilisateurs et data.
Cf gitlab projet “Windy-front” : https://gitlab.com/Marthe-Lo/windy-front

 - BACK-END UTILISATEURS : Java, Spring Boot, MySQL
Le microservice utilisateur est codé en Java/Spring Boot Les données utilisateurs sont stockées sur phpMyAdmin avec des mots de passe cryptés. Enfin, le microservice permet une authentification centrale des autres microservice via un partage des tokens.
Cf gitlab projet “Windy-backJava” : https://gitlab.com/Marthe-Lo/windy-backjava


 - BACK-END DATA : NodeJS, MongoDB Atlas
Ce microservice est développé en NodeJS. Il permet de manipuler les données stockées dans la BDD MongoDB Atlas mais aussi de requêter le microservice utilisateurs via une API REST.. 
Cf gitlab projet “ Windy-back” : https://gitlab.com/Marthe-Lo/windy-back =>  appel API externe NodeJS + cron
Cf gitlab projet “ Windy-back-contract” : https://gitlab.com/Marthe-Lo/windy-back-contract => partie métier => crud des contrats, devis etc

 - ROBOT : API externe, MongoDB Atlas
La base de données MongoDB Atlas est alimentée au moyen d’une API externe (visual crossing) appelée à l’aide d’un robot « cron » codé en NodeJS ou paramétré depuis heroku scheduler.


Déploiement
 - FRONT : AWS S3
 - BACK-END : Heroku


3. Versionnement et automatisation : GitLab & GitLab-CI
