const axios = require("axios");
const express = require("express");
const Router = express.Router();
const lodash = require("lodash");
const Wind = require("../model/wind");
const getIndexField = require("../utils/getIndexField");

Router.get("/winds", (req, res) => {
  axios
    .get(
      "https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/weatherdata/history?aggregateHours=24&combinationMethod=aggregate&startDateTime=2021-01-01T00%3A00%3A00&endDateTime=2021-11-21T00%3A00%3A00&maxStations=-1&maxDistance=-1&sendAsDatasource=true&contentType=json&unitGroup=metric&locationMode=array&key=MXC4C6EMSQ9T7SWULPNHAAUS9&dataElements=default&sourceDatasourceTable=%7B%22name%22%3A%22WxSourceData1%22%2C%22id%22%3A%22WxSourceData1%22%2C%22isPrimary%22%3Atrue%2C%22analyzeLevels%22%3Afalse%2C%22rowDateTimeColumnIndex%22%3A-1%2C%22defaultDateTimeFormat%22%3A%22yyyy-M-d'T'H%3Am%3As%22%2C%22columns%22%3A%5B%7B%22isKey%22%3Atrue%2C%22name%22%3A%22ID%22%2C%22id%22%3A%22id%22%2C%22type%22%3A%22string%22%7D%2C%7B%22isKey%22%3Afalse%2C%22name%22%3A%22Name%22%2C%22id%22%3A%22name%22%2C%22type%22%3A%22string%22%7D%2C%7B%22isKey%22%3Afalse%2C%22name%22%3A%22Address%22%2C%22id%22%3A%22address%22%2C%22type%22%3A%22string%22%7D%5D%2C%22rows%22%3A%5B%5B%22d5a0c0aed7%22%2C%22Flocques%22%2C%22Flocques%22%5D%2C%5B%229b64ac7863%22%2C%22Francourville%22%2C%22Francourville%22%5D%2C%5B%2259237697a6%22%2C%22Maisons-en-Champagne%22%2C%22Maisons-en-Champagne%22%5D%2C%5B%22b547340749%22%2C%22Baronville%22%2C%22Baronville%22%5D%2C%5B%22b5fdd36452%22%2C%22Brem-sur-Mer%22%2C%22Brem-sur-Mer%22%5D%2C%5B%22d05476e65e%22%2C%22Long%C3%A8ves%22%2C%22Long%C3%A8ves%22%5D%2C%5B%2266af94412a%22%2C%22%C2%A0La%20Souterraine%22%2C%22%C2%A0La%20Souterraine%22%5D%2C%5B%2237a1dc0122%22%2C%22Mataud%22%2C%22Mataud%22%5D%2C%5B%220f3d993446%22%2C%22Boll%C3%A8ne%22%2C%22Boll%C3%A8ne%22%5D%2C%5B%222029242bbf%22%2C%22Monts%C3%A9gur%22%2C%22Monts%C3%A9gur%22%5D%2C%5B%22708aa2691a%22%2C%22Joncels%22%2C%22Joncels%22%5D%2C%5B%22e95a20988a%22%2C%22Beaucaire%22%2C%22Beaucaire%22%5D%2C%5B%2287ebed8d8e%22%2C%22%C2%A0Fos-sur-mer%22%2C%22%C2%A0Fos-sur-mer%22%5D%2C%5B%22dc74176f1e%22%2C%22%C2%A0Saint-Nicolas-des-Biefs%22%2C%22%C2%A0Saint-Nicolas-des-Biefs%22%5D%2C%5B%22fee762218b%22%2C%22Chazeuil%22%2C%22Chazeuil%22%5D%2C%5B%22aa91d502b7%22%2C%22Marcellois%22%2C%22Marcellois%22%5D%2C%5B%2242a88f9b5c%22%2C%22Saint-Saury%22%2C%22Saint-Saury%22%5D%2C%5B%22d209558232%22%2C%22Gironville%22%2C%22Gironville%22%5D%2C%5B%22494c6a6dcd%22%2C%22%C2%A0Laprugne%22%2C%22%C2%A0Laprugne%22%5D%2C%5B%222f4623af77%22%2C%22Saint-Cl%C3%A9ment%22%2C%22Saint-Cl%C3%A9ment%22%5D%5D%2C%22layerDataContext%22%3A%7B%22FieldJoins%22%3A%22%7B%7D%22%2C%22useGeoJsonGeometry%22%3A%22true%22%2C%22attributeId%22%3A%22id%22%2C%22shapeType%22%3A%221%22%2C%22onDemandTileGeneration%22%3A%22true%22%2C%22joinLayerColumns%22%3A%22address%22%2C%22contextType%22%3A%226%22%2C%22attributeName%22%3A%22%22%2C%22FieldValues%22%3A%22%7B%7D%22%2C%22addressFields%22%3A%22address%22%7D%7D"
    )
    .then((data) => {
      var success = true;
      lodash.forEach(data.data.locations, (elem) => {
        var params = {
          city_id: elem.id,
          city_name: elem.name,
          city_address: elem.address,
          city_index: elem.index,
          city_latitude: elem.latitude,
          city_longitude: elem.longitude,
          city_index: elem.index,
          city_timeZone: elem.tz,

          temperature: getIndexField(elem, "temp"),
          temperatureMax: getIndexField(elem, "maxt"),
          temperatureMin: getIndexField(elem, "mint"),
          visibility: getIndexField(elem, "visibility"),

          windSpeed: getIndexField(elem, "wspd"),
          windGust: getIndexField(elem, "wgust"),
          windchill: getIndexField(elem, "windchill"),

          datetimeStr: getIndexField(elem, "datetimeStr"),
          datetime: getIndexField(elem, "datetime"),

          cloudcover: getIndexField(elem, "cloudcover"),
          precip: getIndexField(elem, "precip"),
          weathertype: getIndexField(elem, "weathertype"),
          humidity: getIndexField(elem, "humidity"),
          conditions: getIndexField(elem, "conditions"),
          alerts: elem.alerts,
        };

        let newWind = new Wind({
          city_id: params.city_id,
          city_name: params.city_name,
          city_address: params.city_address,
          city_index: params.city_index,
          city_latitude: params.city_latitude,
          city_longitude: params.city_longitude,
          city_index: params.city_index,
          city_timeZone: params.city_timeZone,

          temperature: params.temperature,
          temperatureMax: params.temperatureMax,
          temperatureMin: params.temperatureMin,
          visibility: params.visibility,

          windSpeed: params.windSpeed,
          windGust: params.windGust,
          windchill: params.windchill,

          datetimeStr: params.datetimeStr,
          datetime: params.datetime,

          cloudcover: params.cloudcover,
          precip: params.precip,
          weathertype: params.weathertype,
          humidity: params.humidity,
          conditions: params.conditions,
          alerts: params.alerts,
        });

        Wind.findOne({ datetimeStr: newWind.datetimeStr }, (err, wind) => {
          if (err) console.log("error", err);
          else if (wind) {
            console.log("wind already exist");
          } else {
            newWind.save((err, windObject) => {
              if (err) {
                success = false;
              } else success = true;
            });
          }
        });
      });
      if (!success) res.status(400).json({ err: "failed" });
      else
        res.status(201).json({
          message: "data success",
        });
    })
    .catch((err) => {
      console.log("Erreur", err);
    });
});

module.exports = Router;
