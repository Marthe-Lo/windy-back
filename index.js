const express = require("express"),
  mongoose = require("mongoose"),
  config = require("config"),
  bodyParser = require("body-parser"),
  cron = require("node-cron"),
  saveDataToDB = require("./utils/saveDataToDB");

app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
require("./middleware/cors")(app);

/* routes  */

app.use("/api", require("./controller/windController"));

/* crone */

try {
  cron.schedule(
    // `0 */59 * * * *`,
    // `*/2 * * * *`,
    // `* * * * *`,
    () => {
      saveDataToDB();
    },
    {
      scheduled: true,
      timezone: "Europe/Paris",
    }
  );
} catch (err) {
  console.log(err);
}

mongoose
  .connect(config.get("dbConfig"), { useNewUrlParser: true })
  .then(() => {
    /* "dbConfig": "mongodb://localhost:27017/visualCrossing" */
    console.log("Connexion à MongoDB réussie");
    app.listen(config.get("port"), () => {
      console.log("server is connected on port :", config.get("get"));
    });
  })
  .catch((err) => {
    console.log("La connexion a échoué", err);
  });
