const cors = require("cors");

module.exports = function (app) {
  app.use(cors());
};

/*
en js 

function nomdefct(){

}

nomdefct est facultatif

arrow function 

()=>{

}

*/
