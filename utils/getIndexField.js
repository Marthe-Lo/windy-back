const moment = require("moment");
const lodash = require("lodash");

module.exports = function getIndexField(elem, field) {
  const res = elem.values[elem.values.length - 1][field];
  return res;
};
